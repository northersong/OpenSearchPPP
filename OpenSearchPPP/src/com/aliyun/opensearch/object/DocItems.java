/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.aliyun.opensearch.object;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

/**
 * 一个文档对应的数据
 * 
 * @author 童昭 liushuang.ls@alibaba-inc.com
 */
public class DocItems {
    private List<SingleDoc> docList = new ArrayList<SingleDoc>(); 
    
    /**
     * 添加一行记录
     * @param doc 记录
     */
    public void addDoc(SingleDoc doc){
        this.docList.add(doc);
    }
    
    public String getJSONString(){
        JSONArray array = new JSONArray();
        for(SingleDoc doc : docList){
            array.put(doc.getJSONObject());
        }
        return array.toString();
    }
}
