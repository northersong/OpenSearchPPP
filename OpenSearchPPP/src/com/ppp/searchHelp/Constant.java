package com.ppp.searchHelp;

import com.ppp.searchHelp.util.AppConfig;

/**
 * 常量
 * 
 * @author 刘雄伟
 *
 */
public class Constant {

	/**
	 * 接口连接
	 */
	public static final String OPENSEARCHURL = "http://opensearch.aliyuncs.com";

	/**
	 * accesskey
	 */
	public static final String ACCESSKEY = AppConfig.getString("accesskey");

	/**
	 * secret
	 */
	public static final String SECRET = AppConfig.getString("secret");
	
	
	public static final String tableName = AppConfig.getStr("tableName","main");

	public enum status {

		OK("ok"), FIAL("fial");

		private String value;

		private status(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}
}
