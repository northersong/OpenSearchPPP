package com.ppp.searchHelp.core;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONObject;

import com.aliyun.opensearch.CloudsearchClient;
import com.aliyun.opensearch.CloudsearchDoc;
import com.aliyun.opensearch.CloudsearchIndex;
import com.aliyun.opensearch.CloudsearchSearch;
import com.aliyun.opensearch.object.KeyTypeEnum;
import com.ppp.searchHelp.Constant.status;
import com.ppp.searchHelp.util.AppConfig;

/**
 * PPP插件核心类
 * 
 * 单例CloudsearchClient
 * 
 * 
 * 鄙人norhern也，若汝测出bug，无妨，请务必告知，我要100种方法改掉它！你若要一测到底，我比当奉陪！当然，能给出建议，那norhern在此多谢了，他日，必有重谢。
 * 记住，e-mail and QQ 是: song571377@qq.com
 * 
 * @author song song571377@qq.com
 * @createDate 2015/10/02
 */
public class PPPCore {

	private static PPPCore me = new PPPCore();

	private PPPCore() {
		load();
	}

	public static PPPCore getMe() {
		return me;
	}

	private String ACCESSKEY = AppConfig.getString("accesskey");
	private String SECRET = AppConfig.getString("secret");
	private String host = AppConfig.getString("host");
	private String appName = AppConfig.getString("indexName");
	private Integer maxConectionNum = AppConfig.getInteger("maxConectionNum", null);

	// 保存上次操作时的错误信息
	private String LastError;

	private CloudsearchClient client;

	private void load() {
		Map<String, Object> opts = new HashMap<String, Object>();
		try {
			client = new CloudsearchClient(ACCESSKEY, SECRET, host, opts, KeyTypeEnum.ALIYUN);

			if (maxConectionNum != null && maxConectionNum > 0)
				client.setMaxConnections(maxConectionNum);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public CloudsearchIndex getSearchIndex() {
		return new CloudsearchIndex(appName, client);
	}

	// 检查应用是否可用
	public boolean checkAppStatus() {
		boolean flag = true;
		try {
			String result = getMe().getSearchIndex().status();

			if (!status.OK.getValue().equalsIgnoreCase((new JSONObject(result).getString("status")))) {
				LastError = result;
				flag = false;
			}

		} catch (ClientProtocolException e) {
			LastError = "网络错误";
			flag = false;
			e.printStackTrace();
		} catch (IOException e) {
			LastError = "未知错误";
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	public CloudsearchDoc getCloudsearchDoc() {
		return new CloudsearchDoc(getMe().getAppName(), getMe().getClient());
	}
	
	/**
	 * CloudsearchSearch 是非线程安全。每次都是新建个实例
	 */
	public CloudsearchSearch getCloudsearchSearch(){
		return new CloudsearchSearch(getClient());
	}

	public String getACCESSKEY() {
		return ACCESSKEY;
	}

	public String getSECRET() {
		return SECRET;
	}

	public String getHost() {
		return host;
	}

	public String getAppName() {
		return appName;
	}

	public int getMaxConectionNum() {
		return maxConectionNum;
	}

	public CloudsearchClient getClient() {
		return client;
	}

	public String getLastError() {
		return LastError;
	}

}
