package cn.ppp.test.core;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import com.aliyun.opensearch.CloudsearchSearch;
import com.ppp.searchHelp.core.CloudsearchSearchHelp;
import com.ppp.searchHelp.core.PPPCore;

public class SearchHelpTest {

	//@Test
	public void testexec() throws ClientProtocolException, UnknownHostException, IOException{
		
		JSONObject obj = new JSONObject();
		obj.put("query", "default:'title'");
		obj.put("format", "json");
		
		System.out.println(CloudsearchSearchHelp.execSearch(obj));
	}
	

	@Test
	public void search(){
		CloudsearchSearch search = PPPCore.getMe().getCloudsearchSearch();
		Map<String, Object> opts = new HashMap<>();
		opts.put("query", "default:'s'");
		opts.put("format", "json");
		Map<String, Object> configMap= new HashMap<>();
		configMap.put("start", 20);
		configMap.put("rerank_size",10);
		
		opts.put("config", configMap);
		
		List<String> list = new ArrayList<>();
		list.add("PPPOpenSearch");
		opts.put("indexes",list);
		try {
			String result = search.search(opts);
			System.out.println(result);
			JSONObject jsonObj = new JSONObject(result);
			JSONArray array = jsonObj.getJSONObject("result").getJSONArray("items");
			System.out.println(array.length());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
}
