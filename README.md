#OpenSearchPPP

**简介**
该项目使用java语言开发，可应用与javaWeb项目，支持servlet，SpringMVC等各种java框架，使用方法简单，把源码打jar包引入项目中。配置下config.properties文档，在前端引入js即可。数据源可以在配置文件中配置数据库连接以及sql语句，可以定时添加、修改、删除文档。也可以使用封装的PPPCore类获取操作文档帮助类，自行深入开发。

**使用说明**
1.如上简介所说，源码打jar包引入，引入js文件（OpenSearchPPP.js和ppp.jsp必须在同一目录下，并且项目中ajax实现使用jquery框架，所以，你也需要引入jquery）。改写配置文件，按照注释配置即可(可以下载OpenSerachPPP-demo.7z，该DEMP中含有打好的jar包)
2.数据源可以使用该项目中的定时更新机制，需要数据库表有标识索引状态字段，也可调用CloudsearchDocHelp自行上传文档。
3.页面搜索参数格式参考[OpenSearchAPI搜索参数规格](http://https://docs.aliyun.com/?spm=5176.7393634.9.7.SJLU6O#/pub/opensearch/api-reference/api-interface&search-related)。
4.**注意** 如发现BUG,无妨，请务必告知，我要100种方法改掉它！你若要一测到底，我必当奉陪！当然，能给出建议，那在此多谢了，他日，必有重谢。


**项目结构**
如图**com.ppp.searchHelp.core** 包下
1. Action为ajax搜索实现类
2. PPPCore为核心类，包含 ACCESSKEY等配置信息
3. 其他为搜索和添加文档辅助类
**com.ppp.searchHelp.DB **包下 定时扫描数据库任务类和数据库操作类

**config.properties** 文件包含所有配置信息
```
accesskey = accesskey 
secret = secret 
#搜索应用名称
indexName = PPPOpenSearch
#httpclent最大连接数
maxConectionNum = 10
#阿里api
host = http://opensearch-cn-beijing.aliyuncs.com


#####################
#以下配置如果没用到可以不用配置##
#####################
#定时读取数据库跟新索引
#开启定时任务设置为true
openTask = true
#开始时间yyyy-MM-dd HH:mm:ss 开始时间和延时 只能用一个，两个都存在时默认使用延时时间
startDate = 2015-10-05 02：40：00
#delay 延时 单位秒
delay = 1
#间隔 秒
period = 120

#数据库连接配置
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url = jdbc:mysql://127.0.0.1:3306/test2?useUnicode=true&allowMultiQueries=true&characterEncoding=utf-8
jdbc.username=root
jdbc.password=root

#添加文档SQL
add = select id, name as title,content as body,autho as author from test where `index` = 'add'
#更新文档SQL
update = select id, name as title,content as body,autho as author from test where `index` = 'update'
#移除文档SQL
delete = select id from test where `index` = 'delete'
#更新成功后标记数据SQL
updateIndexStatusSQL = update test set `index`=? where id =?
#更新成功后标记数据的状态
status = ok
```

**js引用示例：**
```
/**
 * 初始化 search插件，可以在初始化时搜索
 * $.PPPSearch(query,optons)
 * query 搜索子句，optons配置信息（过滤，config，聚合等）
 */
var search = $.PPPSearch({
	page:{
		pagerSize:15
	},
	onSuccess : function(data){
		$("#showResult").val(JSON.stringify(data));
	},
	onError:function(data){
		$("#showerrorResult").val(JSON.stringify(data));
	},
	allowNullQuery:true
});

/**
 * 某乃norhern，若汝测出bug，无妨，请务必告知，我要100种方法改掉它！你弱要一测到底，我比当奉陪！当然，能给出建议，那norhern在此多谢了，他日，必有重谢
 * song571377@qq.com
 */
$("#searchBtn").click(function(){
	search.search($("#searchText").val());
});
$("#prv").click(function(){
	search.page(search.options.page.pageNum-1);
});
$("#next").click(function(){
	search.page(search.options.page.pageNum+1);
});
$("#jump").click(function(){
	search.page($("#pageNum").val());
});
$("#refresh").click(function(){
	search.refresh();
});
</script>
```

**页面DEMO就是index.jsp文件。最近工作有点忙，这2天晚上弄的，不完善的地方请指正，谢谢~**


![输入图片说明](http://git.oschina.net/uploads/images/2015/1005/051047_3ae4e785_125728.png "项目结构")